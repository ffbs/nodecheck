package main

import (
	"time"
	"regexp"
	"log"
	"strconv"
	"io"
	"errors"
	"os"
	"fmt"
	"bufio"
)


type Entry struct {
	SourceIP string
	Timestamp time.Time
	Action string
	Path string
	Returncode uint16
}

func (ent Entry) String() string {
	return fmt.Sprintf("%s, %s, %s, %s, %d", ent.SourceIP, ent.Timestamp, ent.Action, ent.Path, ent.Returncode)
}

var entryLineRegex = regexp.MustCompile(`(.*?) .*? .*? \[(.*?)\] "(.*?) (.*?) HTTP/.*?" (\d+)`)
func ParseEntry(line string) (Entry, error) {
	entries := entryLineRegex.FindStringSubmatch(line)
	if entries == nil {
		return Entry{}, fmt.Errorf("Couldn't parse Nginx access entry: '%s'", line)
	}
	timestamp, err := time.Parse("02/Jan/2006:15:04:05 -0700", entries[2])
	retcode, err := strconv.ParseUint(entries[5], 10, 16)
	if err != nil {
		return Entry{}, err
	}
	return Entry{
		SourceIP: entries[1],
		Timestamp: timestamp,
		Action: entries[3],
		Path: entries[4],
		Returncode: uint16(retcode),
	}, nil
}

func ParseAccessLog(path string) (chan Entry, error) {
	fd, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	_, err = fd.Seek(0, 2)
	if err != nil {
		return nil, err
	}

	entries := make(chan Entry)
	go func() {
		defer fd.Close()
		defer close(entries)
		bio := bufio.NewReader(fd)
		for {
			line, err := bio.ReadString('\n')
			if err != nil {
				if !errors.Is(err, io.EOF) {
					// dirty, but better than searching why things don't work
					panic(err)
				}
				// wait for new entries in the log
				time.Sleep(1 * time.Second)
				continue
			}

			ent, err := ParseEntry(line)
			if err != nil {
				log.Printf("Error parsing Nginx entry: %s", err)
				continue
			}
			entries <- ent
		}
	}()

	return entries, nil
}
