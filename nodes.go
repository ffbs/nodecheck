package main

import (
	"encoding/json"
	"os"
	"time"
)

type MyTime time.Time

func (mt *MyTime) UnmarshalJSON(value []byte) error {
	var str string
	if err := json.Unmarshal(value, &str); err != nil {
		return err
	}
	t, err := time.Parse("2006-01-02T15:04:05-0700", str)
	*mt = MyTime(t)
	return err
}

type NodeList struct {
	Timestamp MyTime
	Nodes []*Node
}

type Node struct {
	FirstSeen MyTime
	LastSeen MyTime
	Flags struct {
		Online bool
		Gateway bool
	}
	NodeInfo NodeInfo
}

type NodeInfo struct {
	NodeID string `json:"node_id"`
	Hostname string
	Network struct {
		Addresses []string
	}
	Software struct {
		Autoupdater struct {
			Enabled bool
			Branch string
		}
		Firmware struct {
			Base string
			Release string
		}
	}
	Hardware struct {
		NProc uint32
		Model string
	}
}

func ParseNodeFile(path string) (NodeList, error) {
	fd, err := os.Open(path)
	if err != nil {
		return NodeList{}, err
	}
	defer fd.Close()

	var list NodeList
	if err := json.NewDecoder(fd).Decode(&list); err != nil {
		return NodeList{}, err
	}
	return list, nil
}
