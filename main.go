package main

import (
	"fmt"
	"strings"
	"time"
)

var NodeIDToNode map[string]*Node
var IPToNode map[string]*Node
func updateNodeList() {
	nodelist, err := ParseNodeFile("/srv/yanic/pub/nodes.json")
	if err != nil {
		panic(err)
	}

	newNodeIDToNode := make(map[string]*Node)
	newIPToNode := make(map[string]*Node)
	for _, node := range nodelist.Nodes {
		for _, ip := range node.NodeInfo.Network.Addresses {
			newIPToNode[ip] = node
		}
		newNodeIDToNode[node.NodeInfo.NodeID] = node
	}
	IPToNode = newIPToNode
	NodeIDToNode = newNodeIDToNode
}

func isUpgradeRequest(ent Entry) bool {
	return strings.HasSuffix(ent.Path, ".bin") && strings.Contains(ent.Path, "/sysupgrade/")
}

func watchNodeUpgrade(node *Node) {
	time.Sleep(60 * time.Second)
	node = NodeIDToNode[node.NodeInfo.NodeID]
	for i := 0; i < 2; i++ {
		time.Sleep(60 * time.Second)
		if time.Time(NodeIDToNode[node.NodeInfo.NodeID].LastSeen).After(time.Time(node.LastSeen)) {
			break
		}
	}
	if time.Time(NodeIDToNode[node.NodeInfo.NodeID].LastSeen).After(time.Time(node.LastSeen)) {
		if NodeIDToNode[node.NodeInfo.NodeID].NodeInfo.Software.Firmware.Release != node.NodeInfo.Software.Firmware.Release {
			fmt.Println("Node", node.NodeInfo.Hostname, node.NodeInfo.NodeID, "looks to have upgraded very fast")
		} else {
			fmt.Println("Node", node.NodeInfo.Hostname, node.NodeInfo.NodeID, "didn't initiate a update ==> ignoring")
		}
		return
	}
	fmt.Println("Node", node.NodeInfo.Hostname, node.NodeInfo.NodeID, "gone offline")
	for i := 0; i < 15; i++ {
		if time.Time(NodeIDToNode[node.NodeInfo.NodeID].LastSeen).After(time.Time(node.LastSeen)) {
			break
		}
		time.Sleep(60 * time.Second)
	}
	if time.Time(NodeIDToNode[node.NodeInfo.NodeID].LastSeen).After(time.Time(node.LastSeen)) {
		fmt.Println("Node", node.NodeInfo.Hostname, node.NodeInfo.NodeID, "updated from", node.NodeInfo.Software.Firmware.Release, "to", NodeIDToNode[node.NodeInfo.NodeID].NodeInfo.Software.Firmware.Release)
	} else {
		fmt.Println("Node", node.NodeInfo.Hostname, node.NodeInfo.NodeID, "looks to have failed it's autoupdate")
	}
}

func main() {
	entries, err := ParseAccessLog("/var/log/nginx/access.log.1")
	if err != nil {
		panic(err)
	}

	updateNodeList()
	go func() {
		for {
			time.Sleep(10 * time.Second)
			updateNodeList()
		}
	}()


	fmt.Println("All your codebase belong to us")

	for entry := range entries {
		node, ok := IPToNode[entry.SourceIP]
		if !ok {
			// some irrelevant client...
			continue
		}

		if !isUpgradeRequest(entry) {
			continue
		}

		fmt.Println(entry.Timestamp, "Found upgrade request for", node.NodeInfo.Hostname, node.NodeInfo.NodeID, entry.Path)
		go watchNodeUpgrade(node)
	}
}
